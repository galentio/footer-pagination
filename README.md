# Footer Pagination
To install simply run on root:

* composer install
* cp .env.example .env
* php artisan key:generate

Then configure the default pagination values in the .env file, if you want to use config file values erase the lines in the file
