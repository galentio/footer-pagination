<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Validator;
use Log;

trait FooterPagination
{
    /**
     * Generate footer pagination array
     *
     * @param  integer $current Current page
     * @param  integer $total Total number of pages
     * @param  integer $boundaries Number of page links to show at start and end of pagination
     * @param  integer $current Number of page links to show before and after current page
     * @return array
     */
    public function generateFooterPagination($current, $total, $boundaries, $around): array
    {
        $paginationResult['current_page'] = $current;
        $paginationResult['total_pages'] = $total;
        $paginationResult['boundaries'] = $boundaries;
        $paginationResult['around'] = $around;
        $paginationResult['items'] = array();

        //Note: numbers that are too big will fail validation due to system limitations
        $validator = Validator::make($paginationResult, [
            'current_page' => 'required|integer|min:1|max:'.$total,
            'total_pages' => 'required|integer||min:1',
            'boundaries' => 'required|integer|min:0',
            'around' => 'required|integer|min:0'
        ]);

        //validate pagination parameters, return empty array on error
        if ($validator->fails()) {
            Log::warning('Footer pagination parameters invalid', $validator->errors()->all());
            return array();
        }

        for ($i = 1; $i <= $total; $i++) {
            //check if page will be shown in paginator, ifs with same end result separated for readability
            if ($i == $paginationResult['current_page']) {
                $paginationResult['items'][] = $i;
            } else if ($i <= $boundaries || $i > ($total - $boundaries)) {
                $paginationResult['items'][] = $i;
            } else if ($i >= ($current - $around) && $i <= ($current + $around)) {
                $paginationResult['items'][] = $i;
            } else if (end($paginationResult['items']) != '...') {
                $paginationResult['items'][] = '...';
            }
        }

        return $paginationResult;
    }

    /**
     * Get footer pagination array from Request object
     *
     * @param  Request $request Request object to extract pagination parameters from
     * @return array
     */
    public function getFooterPagination(Request $request): array
    {
        //Get parameters from request, if parameter not found it will default to configurable values
        $parameters['current_page'] = $request->get('current_page', 1);
        $parameters['total_pages'] = $request->get('total_pages', config('footer.pagination_default.total_pages'));
        $parameters['boundaries'] = $request->get('boundaries', config('footer.pagination_default.boundaries'));
        $parameters['around'] = $request->get('around', config('footer.pagination_default.around'));

        return $this->generateFooterPagination($parameters['current_page'], $parameters['total_pages'], $parameters['boundaries'], $parameters['around']);
    }
}
