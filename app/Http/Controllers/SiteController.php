<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\FooterPagination;

class SiteController extends Controller
{
    use FooterPagination;

    /**
     * Get homepage
     *
     * @param  Request  $request
     * @return view
     */
    public function index(Request $request)
    {
        $footer_pagination = $this->getFooterPagination($request);

        return view('home', compact('footer_pagination'));
    }
}
