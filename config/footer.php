<?php

return [
    'pagination_default' => [
        'total_pages'   => env('FOOTER_PAGINATION_TOTAL_PAGES_DEFAULT', 30),
        'boundaries'    => env('FOOTER_PAGINATION_BOUNDARIES_DEFAULT', 3),
        'around'        => env('FOOTER_PAGINATION_AROUND_DEFAULT', 3)
    ]
];
