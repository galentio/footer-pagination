<footer class="footer clearfix">
    @if(isset($footer_pagination) && !empty($footer_pagination))
        <h1>POST</h1>
        <ul>
            <form action="{{ url()->current() }}" method="POST">
                @csrf
                <input type="hidden" name="total_pages" value="{{$footer_pagination['total_pages']}}" />
                <input type="hidden" name="boundaries" value="{{$footer_pagination['boundaries']}}" />
                <input type="hidden" name="around" value="{{$footer_pagination['around']}}" />
                @foreach($footer_pagination['items'] as $paginator_item)
                    <li style="display: inline-block;">
                        @if($footer_pagination['current_page'] == $paginator_item)
                            <b>{{ $paginator_item }}</b>
                        @elseif ($paginator_item == '...')
                            {{ $paginator_item }}
                        @else
                            <input type="submit" value="{{$paginator_item}}" name="current_page" />
                        @endif
                    </li>
                @endforeach
            <form>
        </ul>

        <h1>GET</h1>
        <ul>
            @foreach($footer_pagination['items'] as $paginator_item)
                <li style="display: inline-block;">
                    @if($footer_pagination['current_page'] == $paginator_item)
                        <b>{{ $paginator_item }}</b>
                    @elseif ($paginator_item == '...')
                        {{ $paginator_item }}
                    @else
                        <a href="?current_page={{$paginator_item}}&total_pages={{$footer_pagination['total_pages']}}&boundaries={{$footer_pagination['boundaries']}}&around={{$footer_pagination['around']}}">{{ $paginator_item }}</a>
                    @endif
                </li>
            @endforeach
        </ul>
    @endif

    @if(isset($footer_pagination) && empty($footer_pagination))
        <p>Pagination failed, errors written in log</p>
    @endif
</footer>